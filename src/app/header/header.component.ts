import { Component, OnInit } from '@angular/core';
import { MovizzService } from '../services/movizz.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // Variables Declared
  searchTxt: string;

  constructor(
    public movizzServices: MovizzService
  ) { }

  ngOnInit() {
  }


}
