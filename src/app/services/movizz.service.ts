import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { Observable, BehaviorSubject } from 'rxjs';
import { catchError, retry, map } from 'rxjs/operators';

@Injectable()
export class MovizzService {

    httpOption = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };

    baseApiUrl: any = 'https://api.themoviedb.org/3/';
    apiKey: any = '2e6f57084b053c303d798e13a3eb0878';
    constructor(private http: HttpClient) {

    }

    public getSearchList(data): Observable<any> {
        const url = this.baseApiUrl + 'search/movie?api_key=' + this.apiKey + '&query=' + data.query
        + '&page=' + data.pageNo;
        return this.http.get(url).pipe(
            map((result) => {
                return result;
            })
            , catchError((error) => {
                return error;
            }),
            retry(3)
        );
    }

    public getMovieDetails(movieID): Observable<any> {
        const url = this.baseApiUrl + 'movie/' + movieID + '?api_key=' + this.apiKey;
        return this.http.get(url).pipe(
            map((result) => {
                return result;
            })
            , catchError((error) => {
                return error;
            }),
            retry(3)
        );
    }
}
