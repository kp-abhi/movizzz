import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MovizzService } from '../services/movizz.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  // Variable Declared
  imgBaseUrl = 'https://image.tmdb.org/t/p/w500';
  searchObj = {
    txt: '',
    list: [],
    errorMsg: '',
    totalResult: 0,
    pageNo: 1,
    totalPage: 0,
    selectedMovieID: 0,
    searchDone: false
  };

  movieObj;

  constructor(
    public movizzServices: MovizzService
  ) { }

  ngOnInit() {
  }

  // Fucntion to call the Search Api
  searchMovie() {
    this.movieObj = '';
    this.searchObj.searchDone = true;
    this.searchObj.pageNo = 1;
    this.searchObj.list = [];
    const payload = {
      query: this.searchObj.txt,
      pageNo: this.searchObj.pageNo
    };
    this.movizzServices.getSearchList(payload).subscribe(res => {
      if (res) {
        this.searchObj.list.push.apply(this.searchObj.list, res.results);
        this.searchObj.totalPage = res.total_pages;
        this.searchObj.totalResult = res.total_results;
      } else {
        this.searchObj.errorMsg = res;
      }
    });
  }

  // Fucntion to call the Search on enter
  checkForEnter(data) {
    if (data.keyCode === 13) {
      this.searchMovie();
    }
  }

  // Function to load More List With Same Search Query
  loadMore() {
    if (!(this.searchObj.pageNo > this.searchObj.totalPage)) {
      this.searchObj.pageNo++;
    }
    const payload = {
      query: this.searchObj.txt,
      pageNo: this.searchObj.pageNo
    };
    this.movizzServices.getSearchList(payload).subscribe(res => {
      if (res) {
        this.searchObj.list.push.apply(this.searchObj.list, res.results);
        this.searchObj.totalPage = res.total_pages;
        this.searchObj.totalResult = res.total_results;
      } else {
        this.searchObj.errorMsg = res;
      }
    });
  }

  // Fucntion to get the Detailed view for selected Movie
  selectedMovie(movieID) {
    this.searchObj.selectedMovieID = movieID;
    this.movizzServices.getMovieDetails(movieID).subscribe(res => {
      if (res) {
        this.movieObj = res;
      }
    });
  }

}
